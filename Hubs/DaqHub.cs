﻿using System;
using Microsoft.AspNetCore.SignalR;

namespace SignalRChat
{
    public class DaqHub : Hub
    {
        private readonly DAQ _daq;

        public DaqHub() : this(DAQ.Instance) { }

        public DaqHub(DAQ daq)
        {
            _daq = daq;
        }


        /*public void SendUpdate(string connId)
        {
            _daq.Send(connId);
        }*/

        // Method that sends the latest measurement sets for bucket A to client with connecton id connId
        public void SendUpdateA(string connId, int lastUpdated)
        {
            _daq.SendA(connId, lastUpdated);
        }

        // Method that sends the latest measurement sets for bucket A to client with connecton id connId
        public void SendUpdateAll(string connId)
        {
            _daq.SendAll(connId);
        }

        // Method that sends the latest measurement sets for reference unite 1 to client with connecton id connId
        public void SendUpdateRef1(string connId, int lastUpdated)
        {
            _daq.SendRef1(connId, lastUpdated);
        }

        // Method that imports the site specific data and updates all clients with this data
        public void ImportSite()
        {
            _daq.ImportSiteData();
        }

        // Method that connects to the TCP server with bucket A measurements
        public void StartDAQConnectionA(string ipAddress, Int32 port)
        {
            _daq.StartDAQConnectionA(ipAddress, port);
        }

        // Method that connects to the TCP server with bucket A measurements
        public void StartDAQConnectionB(string ipAddress, Int32 port)
        {
            _daq.StartDAQConnectionB(ipAddress, port);
        }

        // Method that connects to the TCP server with bucket A measurements
        public void StartDAQConnectionC(string ipAddress, Int32 port)
        {
            _daq.StartDAQConnectionC(ipAddress, port);
        }

        // Method that connects to the TCP server with reference unit 1 measurements
        public void StartDAQConnectionRef1(string ipAddress, Int32 port)
        {
            _daq.StartDAQConnectionRef1(ipAddress, port);
        }

        // Method that connects to the TCP server with reference unit 1 measurements
        public void StartDAQConnectionRef2(string ipAddress, Int32 port)
        {
            _daq.StartDAQConnectionRef2(ipAddress, port);
        }

        // Method that starts the log server
        public void StartLogServer(string ipAddress, Int32 port)
        {
            _daq.StartLogServer(ipAddress, port);
        }

        // Method that sends a log message to the log client
        public void SendLogMessage(string message)
        {
            _daq.SendLogMessage(message);
        }

        // Method that x, y data ranges for the Test page graphs for all clients
        public void SetRangeHub()
        {
            Clients.All.setRange();
        }

        // Method that x, y data ranges for the bucket pages graphs for all clients
        public void SetBucketRangeHub()
        {
            Clients.All.setBucketRange();
        }

        // Method that disconnects the TCP connection to bucket A measurements server (outdated, needs more work and testing)
        public void DisConnectDAQConnection()
        {
            _daq.DisConnectDAQConnection();
        }

        // Method that updates the site specific plots for all clients
        public void UpdateSite()
        {
            _daq.SendSiteData();
        }
    }
}
