import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { LayoutService } from '../../services/layout.service';

@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.scss'],
})
export class NavMenuComponent implements OnInit {

    subscription: Subscription;
    public layout;

    constructor(private layoutService: LayoutService) {}
    
    ngOnInit() {
        this.subscription = this.layoutService.layout.subscribe(
            layout => { 
                this.layout = layout;
            }
        )
    }
}
