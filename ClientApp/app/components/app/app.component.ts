import { Component, OnInit, Inject } from '@angular/core';
import { LayoutService } from '../../services/layout.service';
// import { FeedService } from '../../services/feed.service';
import { ChartDataService } from '../../services/chart-data.service';
import { Http } from '@angular/http';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    providers: [ChartDataService, LayoutService]
})
export class AppComponent implements OnInit {

    constructor(private http: Http, @Inject('ORIGIN_URL') private originUrl: string) { }

    ngOnInit() {
        
        //this.http.get(this.originUrl + '/api/Connection/GetConnection').subscribe(result => {
        //});
    }
}
