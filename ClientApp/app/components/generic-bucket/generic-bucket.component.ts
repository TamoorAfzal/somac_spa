﻿import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, HostBinding, Inject, ChangeDetectorRef } from '@angular/core';
import { Http } from '@angular/http';
import { LayoutService } from '../../services/layout.service';
// import { FeedService } from '../../services/feed.service';
import { ChartDataService } from '../../services/chart-data.service';
import { ChartData, SignalRConnectionStatus } from '../../interfaces';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'generic-bucket',
    templateUrl: './generic-bucket.component.html',
    styleUrls: ['./generic-bucket.component.scss']
})

export class GenericBucketComponent implements OnInit {

    public layout = 'layout1';
    subscription: Subscription;    
    data: Array<any>;
    isRunning: boolean = false;
    isPlotting: boolean = true;
    dataFull = [];
    
    @HostBinding('class') bucketLayout: string = '';

    constructor(
        private route: ActivatedRoute,
        public layoutService: LayoutService,
        http: Http,
        private chartDataService: ChartDataService,
        private chRef: ChangeDetectorRef) {
        this.route.data.subscribe(
            data => {
                this.layoutService.setLayout(data.bucketName);
                this.bucketLayout = data.bucketName;
            }
        );        
    }
    ngOnInit() {
        // Propagate layout to other components through service
        this.route.data.subscribe(
            data => {
                this.layoutService.setLayout(data.bucketName);
                this.bucketLayout = data.bucketName;
            }
        );

        //   Set bucket specific layout based on #
        this.route.fragment.subscribe((fragment: string) => {
            if (!fragment) {
                this.layout = 'layout1';
            } else {
                this.layout = fragment;
            }
        })
        
        this.chartDataService.start(true).subscribe(connectionState => {
            this.chartDataService.startDAQConnection(this.bucketLayout.toUpperCase());
            this.chartDataService.sendUpdateAll(0);
        },
            error => console.log('Error on init: ' + error)
        );
        
        this.chartDataService.testPlotAll.subscribe(match => {
            
            switch (this.bucketLayout.toUpperCase()) {
                case "A": {
                    this.data = match.valuesA;
                    break;
                }
                case "B": {
                    this.data = match.valuesB;
                    break;
                }
                case "C": {
                    this.data = match.valuesC;
                    break;
                }
            }
            //this.chRef.detectChanges();
        });
    }
}

function chartPloting(bucket: string, chartData: ChartData) {
    if(chartData != null) {
        if ((!this.isRunning || chartData.reset) && chartData.positiveCountA) {
            this.isRunning = true;
            if (chartData.valuesA.length > 0) {
                if (chartData.reset) {
                    this.dataFull.length = 0;
                }
                let l = this.dataFull.length;
                this.dataFull.length = l + chartData.valuesA.length;
                for (var i = 0; i < chartData.valuesA.length; i++) {
                    this.dataFull[l + i] = {
                        time: chartData.valuesA[0][i] - chartData.zeroTime,
                        Flow: chartData.valuesA[17][i],
                        Suction: chartData.valuesA[13][i],
                        Heave: -(chartData.valuesA[24][i] - chartData.valuesA[23][i]),
                        Penetration: chartData.valuesA[23][i]
                    };

                    if (this.isPlotting) {
                        //updateDataSets(this.dataFull[l + i]);
                    }
                }
            }
            if (this.isPlotting)
            {

            }
            this.isRunning = false;
        }
    }

}