﻿import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss']
})
export class SetupComponent implements OnInit {
    


  ipaddressA: string = localStorage.getItem("IPAddressA");
  ipaddressB: string = localStorage.getItem("IPAddressB");
  ipaddressC: string = localStorage.getItem("IPAddressC");
  portA: string = localStorage.getItem("PortA");
  portB: string = localStorage.getItem("PortB");
  portC: string = localStorage.getItem("PortC");

  constructor() {}
  ngOnInit() {}

  setBucketConnection(bucket) {
    switch (bucket) {
      case "A":
        {
          localStorage.setItem("IPAddressA", this.ipaddressA);
          localStorage.setItem("PortA", this.portA);
          break;
        }
      case "B":
        {
          localStorage.setItem("IPAddressB", this.ipaddressB);
          localStorage.setItem("PortB", this.portB);
          break;
        }
      case "C":
        {
          localStorage.setItem("IPAddressC", this.ipaddressC);
          localStorage.setItem("PortC", this.portC);
          break;

        }
      }   
  }
}