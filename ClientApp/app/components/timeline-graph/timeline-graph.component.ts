import { Component, ElementRef, OnInit, Input } from '@angular/core';
import Dygraph from 'dygraphs';

@Component({
  selector: 'timeline-graph',
  templateUrl: './timeline-graph.component.html',
  styleUrls: ['./timeline-graph.component.scss']
})
export class TimelineGraphComponent implements OnInit {
    _data = [
        [new Date, 0]
    ];
    _graph: any;

  @Input('data')
  set data(value: any) {
      if (value != undefined) {
          this._data = value;
      }
      
      this.updateGraph();
  }

  constructor(private elementRef: ElementRef) {  }
  ngOnInit() { }
  ngAfterViewInit() {
    var graph = new Dygraph(this.elementRef.nativeElement, this._data, {
          legend: 'never',
          //drawGrid: drawGrid,
          axisLineColor: "#585859",
          colors: ["#b09648"],
          axisLineWidth: 2,
          //labels: ['Time', 'Flow'],
          dateWindow: [0 * 60, 180 * 60],
          valueRange: [-600, 600],
          axes: {
              x: {
                  //drawAxis: drawAxis,
                  axisLabelWidth: 70,
                  axisLabelFormatter: function (d, gran) {
                      return pad(Math.floor(d / 3600) % 24, 2) + ":" + pad(Math.floor(d / 60) % 60, 2) + ":" + pad(Math.floor(d) % 60, 2);
                  }
              }
          }
      });
      this._graph = graph;
  }
  updateGraph() {

      if (this._graph != undefined) {
          this._graph.updateOptions({ 'file': this._data });
      }
  }
}

function pad(num, size) {
  var s = "000000000" + num;
  return s.substr(s.length - size);
}