import { Component, OnInit } from '@angular/core';
import { LayoutService } from '../../services/layout.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {

    subscription: Subscription;
    public layout;

    constructor(private layoutService: LayoutService) {}
    ngOnInit() {
        this.subscription = this.layoutService.layout.subscribe(
            layout => { 
                this.layout = layout;
            }
        )
    }

}
