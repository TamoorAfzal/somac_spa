import { Component, ElementRef, OnInit, Input } from '@angular/core';
import Dygraph from 'dygraphs';

@Component({
    selector: 'suction-penetration-graph',
    templateUrl: './suction-penetration-graph.component.html',
    styleUrls: ['./suction-penetration-graph.component.scss']
})
export class SuctionPenetrationComponent implements OnInit {
    _data: any;
    get data(): any {
        return this._data;
    }

    @Input('data')
    set data(value: any) {
        this._data = value;
        this.Graph(this.elementRef.nativeElement, "", "Suction", true, true);
    }
    
    constructor(private elementRef: ElementRef) { }
    ngOnInit() {
        
    }
    Graph(containerName: string, yLabel: string, xLabel: string, drawGrid: boolean, drawAxis: boolean) {
        
        let graphData = this.data;
        if (graphData == null || !graphData.length)
            return;
        const suction = new Dygraph(containerName, graphData,
            {
                legend: 'never',
                drawGrid: drawGrid,
                axisLineColor: "#585859",
                colors: ["#b09648"],
                axisLineWidth : 2,
                ylabel: yLabel,
                xlabel: xLabel,
                dateWindow: [0 * 60, 180 * 60],
                valueRange: [-600, 600],
                axes: {
                    x: {
                        drawAxis: drawAxis,
                        axisLabelWidth: 70,
                        axisLabelFormatter: function (d, gran) {
                            return pad(Math.floor(d / 3600) % 24, 2) + ":" + pad(Math.floor(d / 60) % 60, 2) + ":" + pad(Math.floor(d) % 60, 2);
                        }
                    }
                }
            });
    }

}

function pad(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length - size);
}