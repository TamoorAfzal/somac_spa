import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class LayoutService {

  layout = new BehaviorSubject('default');

  constructor() {}

  public setLayout(layoutName) {
      this.layout.next(layoutName);
  }
}
