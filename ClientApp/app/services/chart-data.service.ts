import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
// import { DaqHubSignalR, DaqHubProxy, DaqHubClient, DaqHubServer, SignalRConnectionStatus, Point } from '../interfaces';
import { DaqHubSignalR, DaqHubProxy, DaqHubServer, SignalRConnectionStatus, ChartData } from '../interfaces';

@Injectable()
export class ChartDataService {

  /**
   * Purpose of this service is acting as the main client for the DaqHub.
   * It's in charge of connecting to the hub and and exposing observable methods.
   */

   // Define initial vars
  currentState = SignalRConnectionStatus.Disconnected;
  connectionState: Observable<SignalRConnectionStatus> ;
  setConnectionId: Observable<string>;
  testPlotAll: Observable<ChartData>;

  private connectionStateSubject = new Subject<SignalRConnectionStatus>();
  private setConnectionIdSubject = new Subject<string>();
  private testPlotAllSubject = new Subject<ChartData>();

  // A reference to the server-side
  private server: DaqHubServer;

  constructor(private http: Http) {
    this.connectionState = this.connectionStateSubject.asObservable();
    this.setConnectionId = this.setConnectionIdSubject.asObservable();
    this.testPlotAll = this.testPlotAllSubject.asObservable();
  }

  start(debug: boolean): Observable<SignalRConnectionStatus> {
    // For debugging purposes
    $.connection.hub.logging = debug;

    // The actual client connection to the DaqHub
    let connection = <DaqHubSignalR>$.connection;

    // reference signalR hub named 'daqhub'
    let daqHub = connection.daqHub;
    this.server = daqHub.server;

    // setConnectionId method called by server
    // TODO: Explain me
    daqHub.client.setConnectionId = id => this.onSetConnectionId(id);

    daqHub.client.testPlotAll = match => this.ontestPlotAll(match);

    // start the connection
    $.connection.hub.start()
      .done(response => this.setConnectionState(SignalRConnectionStatus.Connected))
      .fail(error => this.connectionStateSubject.error(error));

    return this.connectionState;
  }

  private setConnectionState(connectionState: SignalRConnectionStatus) {
      console.log('connection state changed to: ' + connectionState);
      this.currentState = connectionState;
      this.connectionStateSubject.next(connectionState);
  }

  // subscription for setConnectionId method, raises when server confirms connection established and returns connection id
  private onSetConnectionId(id: string) {
      this.setConnectionIdSubject.next(id);
  }

  private ontestPlotAll(match: ChartData) {
      this.testPlotAllSubject.next(match);
  }

  public sendUpdateAll(id: number) {
      this.server.sendUpdateAll(id);
  }

  public startDAQConnection(bucket: string) {
      switch (bucket) {
          case "A": {
             this.server.startDAQConnectionA(localStorage.getItem("IPAddressA"), parseInt(localStorage.getItem("PortA")));
              break;
          }
          case "B": {
              this.server.startDAQConnectionB(localStorage.getItem("IPAddressB"), parseInt(localStorage.getItem("PortB")));
              break;
          }
          case "C": {
              this.server.startDAQConnectionC(localStorage.getItem("IPAddressC"), parseInt(localStorage.getItem("PortC")));
              break;
          }

      }
  }

 
}