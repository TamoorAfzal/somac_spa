/* SignalR related interfaces  */

// Explain me:
// Why are we extending signalr?
// ...
export interface DaqHubSignalR extends SignalR {
    daqHub: DaqHubProxy
}

export interface DaqHubProxy {
    client: DaqHubClient;
    server: DaqHubServer;
}

// Define an interface for the signalr client
// These are all the methods the client are able to exchange data throught signalr with the daqhub. 
export interface DaqHubClient {
    setConnectionId: (id: string) => void;
    sitePlot: (Buckling, Cavitation, DNV, Ic, Piping, qc, SoilType, SPT, UF, zTop) => void;
    status: (daqConnectionStatus, tcpClientStatus, count, logStreamStatus) => void;
    SetBucketRange: () => void;
    testPlotAll: (match: ChartData) => void;
}


export interface DaqHubServer {
    subscribe(bucketName: string): void;
    unsubscribe(bucketName: string): void;
    sendUpdateAll(id: number): void
    startDAQConnectionA(ipAddress: string, port: number): void
    startDAQConnectionB(ipAddress: string, port: number): void
    startDAQConnectionC(ipAddress: string, port: number): void
}

export enum SignalRConnectionStatus {
    Connected = 1,
    Disconnected = 2,
    Error = 3
}

/* SOMAC_SPA related interfaces */
export interface Point {
    X: number;
    Y: number;
}

export interface ChartData {
    zeroTime: number;
    valuesA: Array<any>;
    positiveCountA: boolean;
    valuesB: Array<any>;
    positiveCountB: boolean;
    valuesC: Array<any>;
    positiveCountC: boolean;
    valuesRef1: Array<any>;
    positiveCountRef1: boolean;
    valuesRef2: Array<any>;
    positiveCountRef2: boolean;
    reset: boolean;
}
