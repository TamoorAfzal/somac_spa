import { NgModule, enableProdMode } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser'

import { AppComponent } from './components/app/app.component'
import { GenericBucketComponent } from './components/generic-bucket/generic-bucket.component';
import { FooterComponent } from './components/footer/footer.component';
import { LogWindowComponent } from './components/log-window/log-window.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { NgDygraphsModule } from "ng-dygraphs";
import { OverviewComponent } from './components/overview/overview.component';
import { SetupComponent } from './components/setup/setup.component';
import { TestWindowComponent } from './components/test-window/test-window.component';
import { TimelineGraphComponent } from './components/timeline-graph/timeline-graph.component';
import { SuctionPenetrationComponent } from './components/suction-penetration-graph/suction-penetration-graph.component';
import { SuctionxyzComponent } from './components/suction-xyz-graph/suction-xyz-graph.component';
import { ZoomComponent } from './components/zoom/zoom.component';
import { LayoutService } from './services/layout.service';
import { ConfigService } from './services/config.service';
// import { FeedService } from './services/feed.service';
import { ChartDataService } from './services/chart-data.service';

enableProdMode();
export const sharedConfig: NgModule = {
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        GenericBucketComponent,
        FooterComponent,
        LogWindowComponent,
        NavMenuComponent,
        OverviewComponent,
        SetupComponent,
        TestWindowComponent,
        SuctionPenetrationComponent,
        SuctionxyzComponent,
        TimelineGraphComponent,
        ZoomComponent
    ],
    imports: [
        BrowserModule,
        NgDygraphsModule,
        RouterModule.forRoot([
            { path: 'overview', redirectTo: 'overview', pathMatch: 'full' },
            { path: 'bucket-a', component: GenericBucketComponent, data: {'bucketName': 'a'} },
            { path: 'bucket-b', component: GenericBucketComponent, data: {'bucketName': 'b'} },
            { path: 'bucket-c', component: GenericBucketComponent, data: {'bucketName': 'c'} },
            { path: 'log', component: LogWindowComponent },
            { path: 'overview', component: OverviewComponent },
            { path: 'setup', component: SetupComponent },
            { path: 'test', component: TestWindowComponent },
            { path: 'zoom', component: ZoomComponent },
            { path: '**', redirectTo: 'overview' }
        ])
    ],
    providers: [
        ConfigService,
        LayoutService,
        ChartDataService
        // FeedService
    ]
};
