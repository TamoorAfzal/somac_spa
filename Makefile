##################################################################
#### Variables
##################################################################
HOSTNAME=52.1.51.38
KEY_FILE=~/.ssh/ablochs20161012_luke.pem
USER=ec2-user
BASE_DIR=$(shell pwd)

##################################################################
#### Development
##################################################################
docker-build:
	docker build . -t somacs

docker-run:
	docker run -p 5000:5000 somacs -d

docker-run-nginx:
	docker run --name nginx -v $(BASE_DIR)/nginx.conf:/etc/nginx/nginx.conf:ro -d nginx

##################################################################
#### Deployment
##################################################################
build-bin:
	dotnet publish

zip-build:
	rm -rf bin.zip | true
	zip -r bin.zip bin

move-zip-to-host:
	scp -i $(KEY_FILE) Dockerfile $(USER)@$(HOSTNAME):/home/$(USER)
	scp -i $(KEY_FILE) bin.zip $(USER)@$(HOSTNAME):/home/$(USER)

unzip-bin-on-host:
	ssh -i $(KEY_FILE) $(USER)@$(HOSTNAME) 'cd /home/$(USER); unzip -o bin.zip'

build-docker-image-on-host:
	ssh -i $(KEY_FILE) $(USER)@$(HOSTNAME) 'cd /home/$(USER); docker build . -t somacs'

run-docker-image-on-host:
	ssh -i $(KEY_FILE) $(USER)@$(HOSTNAME) 'cd /home/$(USER); docker kill web' | true
	ssh -i $(KEY_FILE) $(USER)@$(HOSTNAME) 'cd /home/$(USER); docker rm web' | true
	ssh -i $(KEY_FILE) $(USER)@$(HOSTNAME) 'cd /home/$(USER); docker run -d -p 5000:5000 --name web somacs'

docker-cleanup:
	ssh -i $(KEY_FILE) $(USER)@$(HOSTNAME) 'docker volume rm $(docker volume ls -qf dangling=true)'
	ssh -i $(KEY_FILE) $(USER)@$(HOSTNAME) 'docker rmi -f $( docker images -a | grep "<none>" | tr -s " " | cut -d " " -f 3)'

deploy-staging: build-bin \
	zip-build \
	move-zip-to-host \
	unzip-bin-on-host \
	build-docker-image-on-host \
	run-docker-image-on-host \
	docker-cleanup