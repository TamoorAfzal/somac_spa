FROM microsoft/dotnet:1.1-runtime
COPY bin/Debug/netcoreapp1.1/publish/ /app
WORKDIR /app
EXPOSE 5000
ENTRYPOINT ["dotnet", "SOMAC_SPA.dll"]
