﻿using System;
using System.Collections.Generic;
using System.Threading;
using ShellMainLib;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Text;
using SOMAC_SPA;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Hubs;

namespace SignalRChat
{

  public class ChartData
  {
    public double zeroTime { get; set; }
    public double[,] valuesA { get; set; }
    public bool positiveCountA { get; set; }

    public double[,] valuesB { get; set; }
    public bool positiveCountB { get; set; }

    public double[,] valuesC { get; set; }
    public bool positiveCountC { get; set; }

    public double[,] valuesRef1 { get; set; }
    public bool positiveCountRef1 { get; set; }

    public double[,] valuesRef2 { get; set; }
    public bool positiveCountRef2 { get; set; }

    public bool reset { get; set; }
  }
  public class DAQ
  {
    // Singleton instance
    private readonly static Lazy<DAQ> _instance = new Lazy<DAQ>(() => new DAQ(Startup.ConnectionManager.GetHubContext<DaqHub>().Clients));

    // Object containing the measurement data from bucket A
    private readonly MeasurementList _dataA = new MeasurementList();

    // Object containing the measurement data from bucket B
    private readonly MeasurementList _dataB = new MeasurementList();

    // Object containing the measurement data from bucket C
    private readonly MeasurementList _dataC = new MeasurementList();

    // Object that contains the measurement data from reference unit 1
    private readonly MeasurementList _dataRef1 = new MeasurementList();

    // Object that contains the measurement data from reference unit 2
    private readonly MeasurementList _dataRef2 = new MeasurementList();

    // Object that contains the tcp connection to the bucket A measurement server
    private readonly TCPconnection _tcpConnectionA = new TCPconnection();

    // Object that contains the tcp connection to the bucket B measurement server
    private readonly TCPconnection _tcpConnectionB = new TCPconnection();

    // Object that contains the tcp connection to the bucket C measurement server
    private readonly TCPconnection _tcpConnectionC = new TCPconnection();

    // Object that contain the tcp connection to the reference unit 1 server
    private readonly TCPconnection _tcpConnectionRef1 = new TCPconnection();

    // Object that contain the tcp connection to the reference unit 2 server
    private readonly TCPconnection _tcpConnectionRef2 = new TCPconnection();

    // Object that contain site specific data
    private readonly SiteData _siteData = new SiteData();

    // Integer to store the index of _dataA when it was last updated
    private int _lastUpdatedA = 0;

    // Integer to store the index of _dataB when it was last updated
    private int _lastUpdatedB = 0;

    // Integer to store the index of _dataB when it was last updated
    private int _lastUpdatedC = 0;

    // Integer to store the index of _dataB when it was last updated
    private int _lastUpdatedRef1 = 0;

    // Integer to store the index of _dataB when it was last updated
    private int _lastUpdatedRef2 = 0;

    // Sets the update interval for sending new measurement data to all clients
    private readonly TimeSpan _updateInterval = TimeSpan.FromMilliseconds(1000);

    // Sets the update interval for sending debugging info to all clients
    private readonly TimeSpan _updateInterval1 = TimeSpan.FromMilliseconds(2000);

    // The timer for updating debug info to clients
    private readonly Timer _timerAll;

    private DAQ(IHubConnectionContext<dynamic> clients)
    {
      Clients = clients;


      _timerAll = new Timer(UpdateMeasurementsAll, null, _updateInterval, _updateInterval);

    }


    public static DAQ Instance
    {
      get
      {
        return _instance.Value;
      }
    }

    private IHubConnectionContext<dynamic> Clients
    {
      get;
      set;
    }

    // Method that connects to the TCP server with bucket A measurements
    public void StartDAQConnectionA(string ipAddress, Int32 port)
    {
      if (!_tcpConnectionA.dataStreamConnected)
      {
        while (!_tcpConnectionA.dataStreamConnected)
        {
          _tcpConnectionA.EstablishConnection(IPAddress.Parse(ipAddress), port);
        }

        Task taskA = new Task(() => _dataA.DataAcquisition(_tcpConnectionA));

        taskA.Start();
      }
    }

    // Method that connects to the TCP server with bucket B measurements
    public void StartDAQConnectionB(string ipAddress, Int32 port)
    {
      if (!_tcpConnectionB.dataStreamConnected)
      {
        while (!_tcpConnectionB.dataStreamConnected)
        {
          _tcpConnectionB.EstablishConnection(IPAddress.Parse(ipAddress), port);
        }

        Task taskB = new Task(() => _dataB.DataAcquisition(_tcpConnectionB));

        taskB.Start();
      }
    }

    // Method that connects to the TCP server with bucket C measurements
    public void StartDAQConnectionC(string ipAddress, Int32 port)
    {
      if (!_tcpConnectionC.dataStreamConnected)
      {
        while (!_tcpConnectionC.dataStreamConnected)
        {
          _tcpConnectionC.EstablishConnection(IPAddress.Parse(ipAddress), port);
        }

        Task taskC = new Task(() => _dataC.DataAcquisition(_tcpConnectionC));

        taskC.Start();
      }
    }

    // Method that connects to the TCP server with reference unit 1 measurements
    public void StartDAQConnectionRef1(string ipAddress, Int32 port)
    {
      if (!_tcpConnectionRef1.dataStreamConnected)
      {
        while (!_tcpConnectionRef1.dataStreamConnected)
        {
          _tcpConnectionRef1.EstablishConnection(IPAddress.Parse(ipAddress), port);
        }

        Task taskRef1 = new Task(() => _dataRef1.DataAcquisition(_tcpConnectionRef1));

        taskRef1.Start();
      }
    }

    // Method that connects to the TCP server with reference unit 2 measurements
    public void StartDAQConnectionRef2(string ipAddress, Int32 port)
    {
      if (!_tcpConnectionRef2.dataStreamConnected)
      {
        while (!_tcpConnectionRef2.dataStreamConnected)
        {
          _tcpConnectionRef2.EstablishConnection(IPAddress.Parse(ipAddress), port);
        }

        Task taskRef2 = new Task(() => _dataRef2.DataAcquisition(_tcpConnectionRef2));

        taskRef2.Start();
      }
    }

    // Method that starts the log server
    public void StartLogServer(string ipAddress, Int32 port)
    {

      _tcpConnectionA.EstablishServer(IPAddress.Parse(ipAddress), port);
    }

    // Method that sends a log message to the log client
    public void SendLogMessage(string message)
    {
      ASCIIEncoding asen = new ASCIIEncoding();


      Int32 length = (Int32)message.Length;

      _tcpConnectionA.logSocket.Send(BitConverter.GetBytes(length));
      _tcpConnectionA.logSocket.Send(asen.GetBytes(message));

    }

    // Method that disconnects the TCP connection to bucket A measurements server (outdated, needs more work and testing)
    public void DisConnectDAQConnection()
    {
      if (_tcpConnectionA.dataStreamConnected)
      {
        _tcpConnectionA.CloseClientConnection();

      }
    }

    // Method that broadcasts all measurements to all clients
    private void UpdateMeasurements(object state)
    {

      BroadcastMeasurementsA(_dataA, "", 1);
    }

    // Method that broadcasts all measurements to all clients
    private void UpdateMeasurementsAll(object state)
    {

      BroadcastMeasurementsAll(_dataA, _dataB, _dataC, _dataRef1, _dataRef2, "", 1);
    }

    // Method that broadcasts debug info to all clients
    private void UpdateStatus(object state)
    {
      bool status = new bool();
      if (_tcpConnectionA.tcpclnt == null)
      {
        status = false;
      }
      else
      {
        status = IsConnectionClosed(_tcpConnectionA.tcpclnt);
      }

      Clients.All.status(_tcpConnectionA.dataStreamConnected, status, _dataA.Count(), _dataRef1.Count(), _tcpConnectionA.logStreamStatus);
    }

    // Method that sends the latest measurement sets for bucket A to client with connecton id connId
    public void SendA(string connId, int lastUpdated)
    {
      //LogMessageToFile(connId);
      BroadcastMeasurementsA(_dataA, connId, 0);
    }

    // Method that sends the latest measurement sets for bucket A to client with connecton id connId
    public void SendAll(string connId)
    {
      //LogMessageToFile(connId);
      BroadcastMeasurementsAll(_dataA, _dataB, _dataC, _dataRef1, _dataRef2, connId, 0);
    }

    // Method that sends the latest measurement sets for reference unite 1 to client with connecton id connId
    public void SendRef1(string connId, int lastUpdated)
    {
      //LogMessageToFile(connId);
      BroadcastMeasurementsRef1(_dataRef1, connId, lastUpdated);
    }

    // Method that imports the site specific data and updates all clients with this data
    public void ImportSiteData()
    {
      this._siteData.ImportSiteData("C:/Temp/ModelResults.csv");
      this._siteData.SiteImported = true;
      SendSiteData();
    }


    // Method that updates the site specific plots for all clients
    public void SendSiteData()
    {
      if (this._siteData.SiteImported)
      {
        LocationDataArray locationDataArray = this._siteData.GetLocationData("DB1038");

        Clients.All.sitePlot(locationDataArray.Buckling,
                             locationDataArray.Cavitation,
                             locationDataArray.DNV,
                             locationDataArray.Ic,
                             locationDataArray.Piping,
                             locationDataArray.qc,
                             locationDataArray.SoilType,
                             locationDataArray.SPT,
                             locationDataArray.UF,
                             locationDataArray.zTop);
      }
    }

    // Method that broadcasts the new measurements from bucket A to all clients or a specific clients 
    private void BroadcastMeasurementsAll(MeasurementList dataA, MeasurementList dataB, MeasurementList dataC, MeasurementList dataRef1, MeasurementList dataRef2, string connId, int update)
    {
      PlotData plotDataA = GeneratePlotData(dataA, update, _lastUpdatedA);
      PlotData plotDataB = GeneratePlotData(dataB, update, _lastUpdatedB);
      PlotData plotDataC = GeneratePlotData(dataC, update, _lastUpdatedC);
      PlotData plotDataRef1 = GeneratePlotData(dataRef1, update, _lastUpdatedRef1);
      PlotData plotDataRef2 = GeneratePlotData(dataRef2, update, _lastUpdatedRef2);

      _lastUpdatedA = plotDataA.Lastupdated;
      _lastUpdatedB = plotDataB.Lastupdated;
      _lastUpdatedC = plotDataC.Lastupdated;
      _lastUpdatedRef1 = plotDataRef1.Lastupdated;
      _lastUpdatedRef1 = plotDataRef2.Lastupdated;

      if (plotDataA.ZeroTime == 0)
      {

      }

      double zeroTime = Math.Min(plotDataA.ZeroTime, Math.Min(plotDataB.ZeroTime, Math.Min(plotDataC.ZeroTime, Math.Min(plotDataRef1.ZeroTime, plotDataRef2.ZeroTime))));
      ChartData chartData = new ChartData() {
        zeroTime = zeroTime,
        valuesA = plotDataA.Values,
        positiveCountA = plotDataA.positiveCount,
        valuesB = plotDataB.Values,
        positiveCountB = plotDataB.positiveCount,
        valuesC = plotDataC.Values,
        positiveCountC = plotDataC.positiveCount,
        valuesRef1 = plotDataRef1.Values,
        positiveCountRef1 = plotDataRef1.positiveCount,
        valuesRef2 = plotDataRef2.Values,
        positiveCountRef2 = plotDataRef2.positiveCount,
        reset = (update == 0)? true : false
      };
      if (update == 0)
      {
        Clients.Client(connId).testPlotAll(chartData);
      }
      else
      {
        Clients.All.testPlotAll(chartData);
      }


    }

    private PlotData GeneratePlotData(MeasurementList data, int update, int lastUpdated)
    {
      PlotData plotData = new PlotData();
      if (data.Count() > 0)
      {
        int l = 30;
        int[] indices = new int[l];

        for (int i = 0; i < l; i++)
        {
          indices[i] = i;
        }


        try
        {
          if (update == 0)
          {
            plotData = data.GetDataSet(indices, 0);
          }
          else
          {
            plotData = data.GetDataSet(indices, lastUpdated);
            lastUpdated = plotData.Lastupdated;
          }

          if (plotData.Values.Length > 0)
          {
            plotData.positiveCount = true;
          }
        }
        catch
        {
          LogMessageToFile("Failed GetDataSet: " + plotData.Lastupdated + " : " + lastUpdated + " : " + data.Count() + "Message: " + data.message);
        }

      }
      return plotData;
    }


    // Method that broadcasts the new measurements from bucket A to all clients or a specific clients 
    private void BroadcastMeasurementsA(MeasurementList data, string connId, int update)
    {
      PlotData plotData = new PlotData();
      if (data.Count() > 0)
      {
        int l = 30;
        int[] indices = new int[l];

        for (int i = 0; i < l; i++)
        {
          indices[i] = i;
        }

        /*indices[0] = 0;
        indices[1] = 17;
        indices[2] = 13;
        indices[3] = 23;
        indices[4] = 24;
        indices[5] = 5;
        indices[6] = 6;
        indices[7] = 7;
        indices[8] = 8;
        indices[9] = 9;
        indices[10] = 10;
        indices[11] = 11;
        indices[12] = 12;*/


        try
        {
          //Stopwatch stopWatch = new Stopwatch();
          //stopWatch.Start();
          if (update == 0)
          {
            plotData = data.GetDataSet(indices, 0);
          }
          else
          {
            plotData = data.GetDataSet(indices, _lastUpdatedA);
            _lastUpdatedA = plotData.Lastupdated;
          }

          //stopWatch.Stop();
          // Get the elapsed time as a TimeSpan value.
          //TimeSpan ts = stopWatch.Elapsed;


          //Clients.All.testPlot(plotData.Values, plotData.ZeroTime);

          if (plotData.Values.Length > 0)
          {
            if (update == 0)
            {
              Clients.Client(connId).testPlotA(plotData.Values, plotData.ZeroTime, true, data.message);
            }
            else
            {
              Clients.All.testPlotA(plotData.Values, plotData.ZeroTime, false, data.message);
            }
            //Clients.Client(connId).testPlotA(plotData.Values, plotData.ZeroTime, false, data.message,ts.Seconds);
            //Clients.All.testPlotA(plotData.Values, plotData.ZeroTime, false, data.message);
          }
        }
        catch
        {
          LogMessageToFile("Failed GetDataSet: " + plotData.Lastupdated + " : " + _lastUpdatedA + " : " + data.Count() + "Message: " + data.message);
        }

      }


    }



    // Method that broadcasts the new measurements from reference unit 1 to all clients or a specific clients 
    private void BroadcastMeasurementsRef1(MeasurementList data, string connId, int lastUpdated)
    {
      PlotData plotDataRef1 = new PlotData();
      if (data.Count() > 0)
      {
        int[] indices = new int[3];

        indices[0] = 0;
        indices[1] = 1;
        indices[2] = 2;


        try
        {
          plotDataRef1 = data.GetDataSet(indices, lastUpdated);
          //_lastUpdated = plotData.Lastupdated;



          //Clients.All.testPlot(plotData.Values, plotData.ZeroTime);

          if (plotDataRef1.Values.Length > 0)
          {

            //Clients.Client(connId).testPlotRef1(plotDataRef1.Values, plotDataRef1.ZeroTime, false, data.message);
            Clients.All.testPlotRef1(plotDataRef1.Values, plotDataRef1.ZeroTime, false, data.message);
          }
        }
        catch
        {
          LogMessageToFile("Ref1: Failed GetDataSet: " + plotDataRef1.Lastupdated + " : " + lastUpdated + " : " + data.Count() + "Message: " + data.message);
        }

      }


    }

    // Method to check whether the TCP connection is closed
    private bool IsConnectionClosed(TcpClient tcp)
    {
      try
      {
        return (tcp.Client.Poll(1, SelectMode.SelectRead) && tcp.Client.Available == 0);
      }
      catch (SocketException) { return true; }
    }

    // Methods for logging debug info
    public string GetTempPath()
    {
      string path = System.Environment.GetEnvironmentVariable("TEMP");
      if (!path.EndsWith("\\")) path += "\\";
      return path;
    }

    public void LogMessageToFile(string msg)
    {
      System.IO.StreamWriter sw = System.IO.File.AppendText(
          "C:\\Temp\\SOMACS\\My Log File.txt");
      try
      {
        string logLine = System.String.Format(
            "{0:G}: {1}.", System.DateTime.Now, msg);
        sw.WriteLine(logLine);
      }
      finally
      {
        sw.Dispose();
      }
    }

  }
}
