# SOMACS .NET Core MVP app with Angular 4 Client

## Setup Mac

### Install .NET core 1.0.4  
https://github.com/dotnet/core/blob/master/release-notes/download-archives/1.0.4-download.md

## Run project locally

### Run the ConsoleAppDotnetCore
(This is the dummy data provider)

in `../ConsoleAppDotnetCore/ConsoleApp` run:
```
dotnet restore
dotnet run
```

TCPServer should now be listening on local ip address for connections (See terminal for actual ip and ports).

### Setup and run Angular Client app

#### Install node modules
```
npm install
```

#### Configure webpack
```
webpack --config webpack.config.vendor.js
```

For development (i.e. *watch* for changes in .html and .scss files) run:
```
webpack -w
```

To build client app run:
```
webpack
```

To compile app for production run:
```
webpack -p
```

#### Run MVC Client
```
dotnet restore
dotnet run
```

You should now be able to visit your favorite browser at `localhost:5000` and see a running application.

## Project structure

### ClientApp
This is the Angular frontend.

The app consists mainly of components and services, which resides in `app/components` and `app/services`. 
Components are the most basic building blocks for the application, which act as "controllers" that bind data, templates and styling.
Services are used for shared data accross components.

#### Routes
Routes are defined in `app.modules.shared.ts`

#### generic-bucket component
This is a shared component for bucket a, b, and c.  
This is one of the main components for the app.  
The component is used on the urls:
- localhost:5000/bucket-a
- localhost:5000/bucket-b
- localhost:5000/bucket-c

#### feed.service.ts
Explain me

#### interfaces.ts
Explain me

### Controllers

#### ApiHubController
Explain me

#### ConnectionController
Establish connection with ConsoleApp

#### HomeController
Explain me

#### MatchesController
Explain me

#### SampleDataController
Explain me

### Core

#### FeedEngine
FeedEngine is configured in startup.cs gets called and spits data to signalR hub through MatchesController

### Hubs

#### Broadcaster
Explain me

#### Daqhub
Explain me
