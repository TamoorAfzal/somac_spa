﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.Concurrent;

namespace ShellMainLib
{
    public class MeasurementList
    {
        public MeasurementList()
        {
            Measurements = new ConcurrentDictionary<int, SensorData>();
            LastUpdated = 0;
        }

        public MeasurementList(MeasurementList other)
        {
            Measurements = other.Measurements;
            LastUpdated = other.LastUpdated;
        }

        public string message { get; set; }

        public ConcurrentDictionary<int, SensorData> Measurements { get; set; }

        public int LastUpdated { get; set; }

        public PlotData GetDataSet(int[] indices, int lastUpdated)
        {

            PlotData plotData = new PlotData();

            // message = "1, ";

            MeasurementList _measurementlist = new MeasurementList(this);

            //message = message + "2, ";

            plotData.Lastupdated = _measurementlist.Measurements.Count;

            //message = message + "3, " + lastUpdated + " : " + plotData.Lastupdated ;

            double[,] values = new double[indices.Length, plotData.Lastupdated - lastUpdated];

            //message = message + "4, " + "indeces:";

            for (int i = lastUpdated; i < plotData.Lastupdated; i++)
            {
                for (int j = 0; j < indices.Length; j++)
                {
                    values[j, i - lastUpdated] = _measurementlist.Measurements[i].SensorReadings[indices[j]];
                }


            }

            plotData.Values = values;


            if (_measurementlist.Measurements.Count == 0)
            {
                plotData.ZeroTime = 0;
            }
            else
            {
                plotData.ZeroTime = _measurementlist.Measurements[0].SensorReadings[0];
            }

            return plotData;

        }

        public void DataAcquisition(TCPconnection TCPconnect)
        {

            Console.WriteLine("Test ´here 0");

            int index = 0;

            while (true)
            {
                Console.WriteLine("Test ´here 1");
                SensorData sensordata = new SensorData();
                sensordata.AddMeasurement(TCPconnect.dataStream, TCPconnect.tcpclnt);

                Console.WriteLine("Test ´here 2");

                if (!sensordata.TcpConnected)
                {
                    break;
                }
                else
                {
                    Measurements.TryAdd(index, sensordata);
                    index++;
                }

            }
        }

        public Int32 Count()
        {
            return Measurements.Count;
        }

        public string GetTempPath()
        {
            string path = System.Environment.GetEnvironmentVariable("TEMP");
            if (!path.EndsWith("\\")) path += "\\";
            return path;
        }

        public void LogMessageToFile(string msg)
        {
            System.IO.StreamWriter sw = System.IO.File.AppendText(
                "C:\\Temp\\SOMACS\\My Log File.txt");
            try
            {
                string logLine = System.String.Format(
                    "{0:G}: {1}.", System.DateTime.Now, msg);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Dispose();
            }
        }

    }
}
