﻿using System;
using System.Net;
using System.Net.Sockets;

namespace ShellMainLib
{
    public class TCPconnection
    {
        public TCPconnection()
        {
            dataStreamConnected = false;
            logStreamConnected = false;
            logStreamStatus = "Log server is not running";
        }

        public TcpClient tcpclnt { get; set; }

        public IAsyncResult results { get; set; }

        public TcpListener tcpServer { get; set; }

        public bool dataStreamConnected { get; set; }

        public bool logStreamConnected { get; set; }

        public string logStreamStatus { get; set; }

        public NetworkStream dataStream { get; set; }

        public Socket logSocket { get; set; }

        public void EstablishConnection(IPAddress ipaddress, Int32 port)
        {
            tcpclnt = new TcpClient();

            try
            {
                tcpclnt.Client.Connect(ipaddress, port);
                dataStreamConnected = true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Connection failed" + e.StackTrace);
                dataStreamConnected = false;
            }

            Console.WriteLine("Connection: " + IsConnectionClosed(tcpclnt));

            if (dataStreamConnected)
            {
                dataStream = tcpclnt.GetStream();
            }

            Console.WriteLine("Connection datastream: " + dataStreamConnected);
        }

        public void EstablishServer(IPAddress ipadress, Int32 port)
        {
            try
            {
                tcpServer = new TcpListener(ipadress, port);

                tcpServer.Start();

                logStreamStatus = "The server is running at port" + port.ToString() + "\n"
                    + "The local End point is : " + tcpServer.LocalEndpoint
                    + "Waiting for connection";

                logSocket = tcpServer.AcceptSocketAsync().Result;
                logStreamStatus = "Connection accepted from " + logSocket.RemoteEndPoint;

                logStreamConnected = true;
            }
            catch (Exception e)
            {
                logStreamStatus = "Error ..." + e.StackTrace;
            }
        }

        public void CloseClientConnection()
        {
            LingerOption lingerOption = new LingerOption(true, 1);

            tcpclnt.LingerState = lingerOption;

            dataStream.Dispose();

            tcpclnt.Dispose();
            dataStreamConnected = false;
        }

        public void CloseServerConnection()
        {
            logSocket.Dispose();
            tcpServer.Stop();
            logStreamConnected = false;
        }

        private bool IsConnectionClosed(TcpClient tcp)
        {
            try
            {
                return (tcp.Client.Poll(1, SelectMode.SelectRead) && tcp.Client.Available == 0);
            }
            catch (SocketException) { return true; }
        }
    }
}
