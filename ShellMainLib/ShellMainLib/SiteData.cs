﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ShellMainLib
{
    public class SiteData
    {
        public ConcurrentDictionary<string, LocationData> SiteList { get; set; }

        public bool SiteImported { get; set; }

        public SiteData()
        {
            SiteList = new ConcurrentDictionary<string, LocationData>();
            SiteImported = false;
        }

        public void ImportSiteData(string file)
        {
            using (var stream = new FileStream(file, FileMode.Open))
            {
                using (StreamReader sr = new StreamReader(stream))
                {
                    string line;
                    Char delimiter = ';';
                    String[] substrings;

                    Console.WriteLine("Test");
                    Console.WriteLine(file);

                    while (sr.Peek() > 0)
                    {
                        line = sr.ReadLine();
                        substrings = line.Split(delimiter);

                        if (substrings[0] == "**soil")
                        {
                            line = sr.ReadLine();
                            substrings = line.Split(delimiter);

                            string locationName = substrings[0];

                            line = sr.ReadLine();
                            line = sr.ReadLine();

                            LocationData locationData = new LocationData();

                            while (sr.Peek() > 0)
                            {
                                line = sr.ReadLine();
                                substrings = line.Split(delimiter);
                                locationData.AddLocationValue(substrings);
                                //Console.WriteLine("Peek {0}", sr.Peek());

                            }


                            this.SiteList.TryAdd(locationName, locationData);
                        }
                    }

                }
            }
            
        }

        public LocationDataArray GetLocationData(string locationName)
        {
            LocationData locationData = new LocationData();

            bool locationExists = this.SiteList.TryGetValue(locationName, out locationData);

            LocationDataArray locationDataArray = new LocationDataArray(locationData.Data.Count);

            for (var i = 0; i < locationData.Data.Count; i++)
            {
                LocationValues values = locationData.Data[i];
                locationDataArray.Buckling[i] = values.Buckling;
                locationDataArray.Cavitation[i] = values.Cavitation;
                locationDataArray.DNV[i] = values.DNV;
                locationDataArray.Ic[i] = values.Ic;
                locationDataArray.Piping[i] = values.Piping;
                locationDataArray.qc[i] = values.qc;
                locationDataArray.SoilType[i] = values.SoilType;
                locationDataArray.SPT[i] = values.SPT;
                locationDataArray.UF[i] = values.UF;
                locationDataArray.zTop[i] = values.zTop;

            }

            return locationDataArray;
        }

    }
}
