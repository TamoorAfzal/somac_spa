﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace ShellMainLib
{
    public class SensorData
    {
        public double[] SensorReadings { get; set; }

        public bool TcpConnected { get; set; }

        public void AddMeasurement(NetworkStream stream, TcpClient tcpclnt)
        {
            Byte[] data = new Byte[4];
            Int32 bytes;


            if (!IsConnectionClosed(tcpclnt))
            {
                bytes = stream.Read(data, 0, data.Length);
                while (bytes == 0 & !IsConnectionClosed(tcpclnt))
                {
                    bytes = stream.Read(data, 0, data.Length);
                }
            }

            if (!IsConnectionClosed(tcpclnt))
            {

                Array.Reverse(data);

                Console.WriteLine("Bites : {0}", BitConverter.ToString(data));

                int ii = BitConverter.ToInt32(data, 0);
                
                // Buffer to store the response bytes.
                data = new Byte[ii];

                bytes = stream.Read(data, 0, data.Length);

                Console.WriteLine("Length : {0}", ii);
                Console.WriteLine("Bites : {0}", BitConverter.ToString(data));

                double[] values = new double[data.Length / 8];
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = BitConverter.ToDouble(data, i * 8);
                    Console.WriteLine("Value {0} : {1}", i, values[i]);
                }

                this.SensorReadings = values;

                this.TcpConnected = true;
            }
            else
            {
                this.TcpConnected = false;
            }




        }

        private bool IsConnectionClosed(TcpClient tcp)
        {
            try
            {
                return (tcp.Client.Poll(1, SelectMode.SelectRead) && tcp.Client.Available == 0);
            }
            catch (SocketException) { return true; }
        }
    }
}
