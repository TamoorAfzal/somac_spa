﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShellMainLib
{
    public class PlotData
    {
        public double[,] Values { get; set; }

        public int Lastupdated { get; set; }

        public double ZeroTime { get; set; }

        public bool positiveCount { get; set; }

        public PlotData()
        {
            positiveCount = false;
            ZeroTime = Double.MaxValue;
        }
    }
}
