﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace ShellMainLib
{
    public class LocationData
    {
        public List<LocationValues> Data { get; set; }

        public LocationData()
        {
            Data = new List<LocationValues>();
        }

        public void AddLocationValue(string[] substrings)
        {
            LocationValues values = new LocationValues();

            //Console.WriteLine("Substrings {0}", substrings[9]);
            CultureInfo usCulture = new CultureInfo("en-US");
            NumberFormatInfo dbNumerFormat = usCulture.NumberFormat;

            values.zTop = double.Parse(substrings[0], dbNumerFormat);
            values.qc = double.Parse(substrings[1], dbNumerFormat);
            values.Ic = double.Parse(substrings[2], dbNumerFormat);
            values.DNV = double.Parse(substrings[3], dbNumerFormat);
            values.SPT = double.Parse(substrings[4], dbNumerFormat);
            values.UF = double.Parse(substrings[5], dbNumerFormat);
            values.Cavitation = double.Parse(substrings[6], dbNumerFormat);
            values.Piping = double.Parse(substrings[7], dbNumerFormat);
            values.Buckling = double.Parse(substrings[8], dbNumerFormat);
            values.SoilType = substrings[9];

            this.Data.Add(values);
        }
    }

    public class LocationDataArray
    {
        public double[] zTop { get; set; }
        public double[] qc { get; set; }
        public double[] Ic { get; set; }
        public double[] DNV { get; set; }
        public double[] SPT { get; set; }
        public double[] UF { get; set; }
        public double[] Cavitation { get; set; }
        public double[] Piping { get; set; }
        public double[] Buckling { get; set; }
        public string[] SoilType { get; set; }

        public LocationDataArray(int l)
        {
            zTop = new double[l];
            qc = new double[l];
            Ic = new double[l];
            DNV = new double[l];
            SPT = new double[l];
            UF = new double[l];
            Cavitation = new double[l]; ;
            Piping = new double[l];
            Buckling = new double[l];
            SoilType = new string[l];
        }
    }
}
