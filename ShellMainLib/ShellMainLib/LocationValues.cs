﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShellMainLib
{
    public class LocationValues
    {
        public double zTop { get; set; }
        public double qc { get; set; }
        public double Ic { get; set; }
        public double DNV { get; set; }
        public double SPT { get; set; }
        public double UF { get; set; }
        public double Cavitation { get; set; }
        public double Piping { get; set; }
        public double Buckling { get; set; }
        public string SoilType { get; set; }
    }
}
